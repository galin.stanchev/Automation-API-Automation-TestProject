﻿using Automation_API_Automation_TestProject.EndPoints;
using Automation_API_Automation_TestProject.HelperClass.Request;
using Automation_API_Automation_TestProject.Models.CirclesGet;
using Automation_API_Automation_TestProject.Models.RequestModels;
using Automation_API_Automation_TestProject.Models.Threads;
using Automation_API_Automation_TestProject.Models.UsersMe;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.UserCases
{
    [TestClass]
    public class ThirdUserCase
    {

        int cNotJoined;
        int threadType;
        int tNum = 1;
        Random random = new Random();
        string tag1;
        string tag2;
        string tag3;
        int threadId;
        string threadTitle = "THREAD Title";
        string threadBody = "THREAD Body";
        int isAnon = 0;

        FirstUserCase firstUserCase = new FirstUserCase();
        SecondUserCase secondUserCase = new SecondUserCase();


        [TestMethod]
        public void CirclesNotJoined()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken}
            };

            RestClientHelper restClientHelper = new RestClientHelper();           
            IRestResponse<CirclesGetRoot> restResponse = restClientHelper.PerformGetRequest<CirclesGetRoot>(EndpointsKeeper.getCirclesNotJoined, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                cNotJoined = restResponse.Data.data[0].id;
            }
        }

        [TestMethod]
        public void JoinSecondCircle()
        {
            string urlWithParam = EndpointsKeeper.postJoinSecondCircle + cNotJoined + "/circle";

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken}
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse restResponse = restClientHelper.PerformPostRequest(urlWithParam, headers, null, DataFormat.Json);           
            Assert.AreEqual(204, (int)restResponse.StatusCode);                      

        }

        [TestMethod]
        public void SecondCircleCheck()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();       
            IRestResponse<UsersMeRoot> restResponse = restClientHelper.PerformGetRequest<UsersMeRoot>(EndpointsKeeper.getUsersMeUrl, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
               Assert.AreEqual(cNotJoined, restResponse.Data.data.joined_mentor_circles[restResponse.Data.data.joined_mentor_circles.Count-1].id);
            }

        }


        [TestMethod]
        public void SecondCircleSecondCheck()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse<CirclesGetRoot> restResponse = restClientHelper.PerformGetRequest<CirclesGetRoot>(EndpointsKeeper.getCirclesGet, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
               Assert.AreEqual(cNotJoined, restResponse.Data.data[0].id);
            }
        }

        [TestMethod]
        public void GetThreadType()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse<CirclesGetRoot> restResponse = restClientHelper.PerformGetRequest<CirclesGetRoot>(EndpointsKeeper.getThreadType, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                threadType = restResponse.Data.data[0].id;                
            }
        }

        public void CreateNewThread()
        {
            string urlWithParam = EndpointsKeeper.getCirclesGet + "/" + tNum + "/threads";

            IRestClient restClient = new RestClient();
            IRestRequest request = new RestRequest()
            {
                Resource = urlWithParam
            };

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", "Bearer " + secondUserCase.MyToken);
            request.AddParameter("type_id", threadType);
            request.AddParameter("title" , threadTitle);
            request.AddParameter("body" , threadBody);
            request.AddParameter("is_anonymous", isAnon);
            request.AddParameter("attachments[links][0]", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");


            IRestResponse<ThreadsRoot> restResponse = restClient.Post<ThreadsRoot>(request);
            Assert.AreEqual(201, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                threadId = restResponse.Data.data.id;                
            }

        }

        [TestMethod]
        public void PutTags()
        {         
            string urlWithParam = EndpointsKeeper.putTags + threadId + "/tags";

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" },
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse restResponse = restClientHelper.PerformPutRequest(urlWithParam, headers, GetTagsObject(), DataFormat.Json);
            Assert.AreEqual(204, (int)restResponse.StatusCode);
           
        }


        [TestMethod]
        public void VerifyTheThreadIsInCorrectCircle()
        {
            string urlWithParam = EndpointsKeeper.putTags + threadId;

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse<ThreadsRoot> restResponse = restClientHelper.PerformGetRequest<ThreadsRoot>(urlWithParam, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                Assert.AreEqual(tNum, restResponse.Data.data.circle.id);                
            }
        }

        [TestMethod]
        public void VerifyAllInfoPossible()
        {
            string urlWithParam = EndpointsKeeper.putTags + threadId;

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + secondUserCase.MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse<ThreadsRoot> restResponse = restClientHelper.PerformGetRequest<ThreadsRoot>(urlWithParam, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                Assert.AreEqual(threadId, restResponse.Data.data.id);
                Assert.AreEqual(threadTitle, restResponse.Data.data.title);
                Assert.AreEqual(threadBody, restResponse.Data.data.body);
                Assert.AreEqual(isAnon, Convert.ToInt32(restResponse.Data.data.is_anonymous));
                Assert.AreEqual(firstUserCase.MyFName, restResponse.Data.data.author.first_name);
                Assert.AreEqual(firstUserCase.MyLName, restResponse.Data.data.author.last_name);               
                Assert.AreEqual(tNum, restResponse.Data.data.circle.id);

            }
        }


        [TestMethod]
        public void MasterMethodFlow()
        {
            secondUserCase.MasterMethodFlow();
            CirclesNotJoined();
            JoinSecondCircle();
            SecondCircleCheck();
            SecondCircleSecondCheck();
            GetThreadType();
            CreateNewThread();
            PutTags();
            VerifyTheThreadIsInCorrectCircle();
            VerifyAllInfoPossible();
        }


        //--------------------------------------------------------------GetObjectMethods------------------------------------------------------------


        private TagsRoot GetTagsObject()
		{

            tag1 = "Tag" + random.Next(10000);
            tag2 = "Tag" + random.Next(10000);
            tag3 = "Tag" + random.Next(10000);

            TagsRoot tagsRoot = new TagsRoot();
            List<string> tagsList = new List<string>()
            {
               (tag1),
               (tag2),
               (tag3)
            };

            tagsRoot.tags = tagsList;

            return tagsRoot;
        }
    }
}
