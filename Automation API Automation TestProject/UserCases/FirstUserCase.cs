﻿using Automation_API_Automation_TestProject.EndPoints;
using Automation_API_Automation_TestProject.HelperClass.Request;
using Automation_API_Automation_TestProject.Model;
using Automation_API_Automation_TestProject.Models.RequestModels;
using Automation_API_Automation_TestProject.Models.RequestModels.Register;
using Automation_API_Automation_TestProject.Models.UsersMe;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Automation_API_Automation_TestProject.UserCases
{
    [TestClass]
    public class FirstUserCase
    {

        private Random random = new Random();

        private int randomEx;


        private string token;
        private string emailAddress;
        private string password = "Qwerty1!";
        private string fName = "Axl";
        private string lName = "Rose";



        public string MyEmailAddress { get => emailAddress; set => emailAddress = value; }
        public string MyPassword { get => password; set => password = value; }
        public string MyFName { get => fName; set => fName = value; }
        public string MyLName { get => lName; set => lName = value; }
    

        [TestMethod]
        public void PostRegisterUserWithInvalidEmail()
        {
            randomEx = random.Next(10000);
            string jsonData = "{" +
                "\"first_name\":\"Axl\"," +
                "\"last_name\":\"Rose\"," +
                "\"email\":\"galin.stanchev+.mentormate.com\"," +
                "\"password\":\"Qwerty1!\"," +
                "\"password_confirmation\":\"Qwerty1!\"" +
                "}";

            IRestClient restClient = new RestClient();
            IRestRequest request = new RestRequest()
            {
                Resource = EndpointsKeeper.postRegisterUrl
            };

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(jsonData);

            IRestResponse<JsonRoot> restResponse = restClient.Post<JsonRoot>(request);
            Assert.AreEqual(422, (int)restResponse.StatusCode);
            StringAssert.Contains(restResponse.Content, "The email must be a valid email address");            
            
        }


        [TestMethod]
        public void PostRegisterUser()
        {

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse<JsonRoot> restResponse = restClientHelper.PerformPostRequest<JsonRoot>(EndpointsKeeper.postRegisterUrl, headers, GetRegisterObject(), DataFormat.Json);
                       
            Assert.AreEqual(201, (int)restResponse.StatusCode);
            Assert.IsFalse(restResponse.Content.Contains(MyPassword));           

            if (restResponse.IsSuccessful)
            {                
                MyEmailAddress = restResponse.Data.data.user.email;                
            }

        }

       

        [TestMethod]
        public void PostLoginUser()
        {
                        
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse<JsonRoot> restResponse = restClientHelper.PerformPostRequest<JsonRoot>(EndpointsKeeper.postLoginUrl, headers, GetLoginObject(), DataFormat.Json);

            Assert.AreEqual(200, (int)restResponse.StatusCode);                  

            if (restResponse.IsSuccessful)
            {
                token = restResponse.Data.data.access_token;
                Assert.AreEqual(MyEmailAddress, restResponse.Data.data.user.email);
                Assert.AreEqual(MyFName, restResponse.Data.data.user.first_name);
                Assert.AreEqual(MyLName, restResponse.Data.data.user.last_name);
                Assert.IsFalse(restResponse.Content.Contains(MyPassword));
            }

        }

        [TestMethod]
        public void GetUsersMe()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + token }
            };

            RestClientHelper restClientHelper = new RestClientHelper();   

            IRestResponse<UsersMeRoot> restResponse = restClientHelper.PerformGetRequest<UsersMeRoot>(EndpointsKeeper.getUsersMeUrl, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                Assert.AreEqual(MyEmailAddress, restResponse.Data.data.email);
                Assert.AreEqual(MyFName, restResponse.Data.data.first_name);
                Assert.AreEqual(MyLName, restResponse.Data.data.last_name); 
            }
           
        }

        [TestMethod]
        public void PostLogout()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + token }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse restResponse = restClientHelper.PerformPostRequest(EndpointsKeeper.postLogout, headers, null, DataFormat.Json);

            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                StringAssert.Contains(restResponse.Content, "Logged out successfully");
                Assert.IsFalse(restResponse.Content.Contains(MyEmailAddress));
                Assert.IsFalse(restResponse.Content.Contains(MyFName));
                Assert.IsFalse(restResponse.Content.Contains(MyLName)); 
            }

        }

        [TestMethod]
        public void MasterMethodFlow()
        {
            PostRegisterUserWithInvalidEmail();
            PostRegisterUser();
            PostLoginUser();
            GetUsersMe();
            PostLogout();
        }




        // --------------------------------------------------GetObjectsMethods-------------------------------------------------------------------------

        private RegisterRoot GetRegisterObject()
        {
            randomEx = random.Next(10000);
            string myemail = "galin.stanchev+" + randomEx + "@mentormate.com";

            RegisterRoot registerRoot = new RegisterRoot();

            registerRoot.first_name = MyFName;
            registerRoot.last_name = MyLName;
            registerRoot.email = myemail;
            registerRoot.password = MyPassword;
            registerRoot.password_confirmation = MyPassword;

            return registerRoot;

        }

        private RegisterRoot GetLoginObject()
        {                   
            RegisterRoot loginRoot = new RegisterRoot();
                     
            loginRoot.email = MyEmailAddress;
            loginRoot.password = MyPassword;
           

            return loginRoot;

        }

    }
}
