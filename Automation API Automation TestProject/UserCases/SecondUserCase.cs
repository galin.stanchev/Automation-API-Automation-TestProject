﻿using Automation_API_Automation_TestProject.EndPoints;
using Automation_API_Automation_TestProject.HelperClass.Request;
using Automation_API_Automation_TestProject.Model;
using Automation_API_Automation_TestProject.Models.CirclesGet;
using Automation_API_Automation_TestProject.Models.PassChange;
using Automation_API_Automation_TestProject.Models.RequestModels;
using Automation_API_Automation_TestProject.Models.RequestModels.Register;
using Automation_API_Automation_TestProject.Models.SetupProfile;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.UserCases
{

    [TestClass]
    public class SecondUserCase
    {

        private string token;
        private string newPass = "ThisIsNew123@";

        private int interest1;
        private int interest2;
        private int interest3;

        string title = "RandomTitle";
        string department = "RandomDepartment";
        string company = "RandomCompany";
        string location = "MiddleOfNowhere";
        string biography = "NoSuch";
        string skills = "Doing Nothing";


        FirstUserCase firstUserCase = new FirstUserCase();

        public string MyToken { get => token; set => token = value; }
        public string MyTitle { get => title; set => title = value; }
        public string MyDepartment { get => department; set => department = value; }
        public string MyCompany { get => company; set => company = value; }
        public string MyLocation { get => location; set => location = value; }
        public string MyBiography { get => biography; set => biography = value; }
        public string MySkills { get => skills; set => skills = value; }

        [TestMethod]
        public void LoginSuccessfully()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse<JsonRoot> restResponse = restClientHelper.PerformPostRequest<JsonRoot>(EndpointsKeeper.postLoginUrl, headers, GetLoginObject(), DataFormat.Json);

            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                MyToken = restResponse.Data.data.access_token;
                Assert.AreEqual(firstUserCase.MyEmailAddress, restResponse.Data.data.user.email);
                Assert.AreEqual(firstUserCase.MyFName, restResponse.Data.data.user.first_name);
                Assert.AreEqual(firstUserCase.MyLName, restResponse.Data.data.user.last_name);
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyPassword));
            }
        }


        [TestMethod]
        public void SetUpProfile()
        {
            string path = @"C:\Users\Axl\Desktop\20131209_220736.jpg";
           

            IRestClient restClient = new RestClient();
            IRestRequest request = new RestRequest()
            {
                Resource = EndpointsKeeper.postSetUpProfile
            };
        
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddHeader("Authorization", "Bearer " + MyToken);

            //request.AddFile("profile_picture", File.ReadAllBytes(path), Path.GetFileName(path), "application/octet-stream");
            request.AddParameter("title", MyTitle);
            request.AddParameter("company", MyCompany);
            request.AddParameter("department", MyDepartment);
            request.AddParameter("location", MyLocation);
            request.AddParameter("biography", MyBiography);
            request.AddParameter("skills", MySkills);         

            IRestResponse<SetupProfileRoot> restResponse = restClient.Post<SetupProfileRoot>(request);
            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {           
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyPassword));
            }
        }

        [TestMethod]
        public void CirclesGet()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
           
            IRestResponse<CirclesGetRoot> restResponse = restClientHelper.PerformGetRequest<CirclesGetRoot>(EndpointsKeeper.getCirclesGet, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                
                interest1 = restResponse.Data.data[5].id;
                interest2 = restResponse.Data.data[6].id;
                interest3 = restResponse.Data.data[7].id;
             
            }
        }

        [TestMethod]
        public void SetUpInterests()
        {

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json"},
                {"Authorization", "Bearer " + MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse restResponse = restClientHelper.PerformPostRequest(EndpointsKeeper.postSetUpInterests, headers, GetInterestsObject(), DataFormat.Json);
            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                StringAssert.Contains(restResponse.Content, "Registration completed.");               
            }

        }


        [TestMethod]
        public void CorrectSavedInfoCheck()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();          

            IRestResponse<CirclesGetRoot> restResponse = restClientHelper.PerformGetRequest<CirclesGetRoot>(EndpointsKeeper.getCorrectSavedInfoCheck, headers);
            Assert.AreEqual(200, (int)restResponse.StatusCode);
            Assert.IsNotNull(restResponse.Data, "Content is Null");

            if (restResponse.IsSuccessful)
            {
                Assert.AreEqual(interest1, restResponse.Data.data[0].id);
                Assert.AreEqual(interest2, restResponse.Data.data[1].id);
                Assert.AreEqual(interest3, restResponse.Data.data[2].id);
            }
        }

        [TestMethod]
        public void PasswordChange()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json"},
                {"Authorization", "Bearer " + MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();

            IRestResponse<PassChangeRoot> restResponse = restClientHelper.PerformPutRequest<PassChangeRoot>(EndpointsKeeper.putPasswordChange, headers, GetPassChangeObject(), DataFormat.Json);
            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                Assert.AreEqual(firstUserCase.MyEmailAddress, restResponse.Data.data.email);
                Assert.AreEqual(firstUserCase.MyFName, restResponse.Data.data.first_name);
                Assert.AreEqual(firstUserCase.MyLName, restResponse.Data.data.last_name);
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyPassword));
                Assert.IsFalse(restResponse.Content.Contains(newPass));
            }
        }

        [TestMethod]
        public void PostLogout()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Authorization", "Bearer " + MyToken }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse restResponse = restClientHelper.PerformPostRequest(EndpointsKeeper.postLogout, headers, null, DataFormat.Json);

            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                StringAssert.Contains(restResponse.Content, "Logged out successfully");
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyEmailAddress));
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyFName));
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyLName));
            }

        }

        [TestMethod]
        public void LoginWithOldPass()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse<JsonRoot> restResponse = restClientHelper.PerformPostRequest<JsonRoot>(EndpointsKeeper.postLoginUrl, headers, GetLoginObject(), DataFormat.Json);
           
            Assert.AreEqual(401, (int)restResponse.StatusCode);

            if (!restResponse.IsSuccessful)
            {               
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyEmailAddress));
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyFName));
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyLName));
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyPassword));
                StringAssert.Contains(restResponse.Content, "Incorrect email or password");
            }


        }

        [TestMethod]
        public void LogWithNewPass()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                {"Content-Type", "application/json"},
                {"Accept", "application/json" }
            };

            RestClientHelper restClientHelper = new RestClientHelper();
            IRestResponse<JsonRoot> restResponse = restClientHelper.PerformPostRequest<JsonRoot>(EndpointsKeeper.postLoginUrl, headers, GetLoginWithNewPassObject(), DataFormat.Json);
          
            Assert.AreEqual(200, (int)restResponse.StatusCode);

            if (restResponse.IsSuccessful)
            {
                MyToken = restResponse.Data.data.access_token;
                Assert.AreEqual(firstUserCase.MyEmailAddress, restResponse.Data.data.user.email);
                Assert.AreEqual(firstUserCase.MyFName, restResponse.Data.data.user.first_name);
                Assert.AreEqual(firstUserCase.MyLName, restResponse.Data.data.user.last_name);
                Assert.IsFalse(restResponse.Content.Contains(firstUserCase.MyPassword));
                Assert.IsFalse(restResponse.Content.Contains(newPass));
            }


        }

        [TestMethod]
        public void MasterMethodFlow()
        {
            firstUserCase.MasterMethodFlow();
            LoginSuccessfully();
            SetUpProfile();
            CirclesGet();
            SetUpInterests();
            CorrectSavedInfoCheck();
            PasswordChange();
            PostLogout();
            LoginWithOldPass();
            LogWithNewPass();
        }




        //----------------------------------------------------------------GetObjectMethods-----------------------------------------------

        private RegisterRoot GetLoginObject()
        {
            RegisterRoot loginRoot = new RegisterRoot();

            loginRoot.email = firstUserCase.MyEmailAddress;
            loginRoot.password = firstUserCase.MyPassword;


            return loginRoot;
        }

        private InterestsModel GetInterestsObject()
        {
            InterestsModel interestsModel = new InterestsModel();
            List<string> interestList = new List<string>()
            {
                ("" + interest1 + ""),
                ("" + interest2 + ""),
                ("" + interest3 + "")
            };

            interestsModel.interests = interestList;

            return interestsModel;
        }

        private RegisterRoot GetPassChangeObject()
		{
            RegisterRoot passChangeRoot = new RegisterRoot();

            passChangeRoot.old_password = firstUserCase.MyPassword;
            passChangeRoot.password = newPass;
            passChangeRoot.password_confirmation = newPass;

            return passChangeRoot;
		}

        private RegisterRoot GetLoginWithNewPassObject()
        {
            RegisterRoot loginRoot = new RegisterRoot();

            loginRoot.email = firstUserCase.MyEmailAddress;
            loginRoot.password = newPass;


            return loginRoot;
        }


    }
}
