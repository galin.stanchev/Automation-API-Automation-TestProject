﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.RequestModels
{
	public class InterestsModel
	{
		public List<string> interests { get; set; }
	}
}
