﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.RequestModels.Register
{
	public class RegisterRoot
	{
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string email { get; set; }

		public string old_password { get; set; }
		public string password { get; set; }
		public string password_confirmation { get; set; }
	}
}
