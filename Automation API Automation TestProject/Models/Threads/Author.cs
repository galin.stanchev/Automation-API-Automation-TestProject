﻿using Automation_API_Automation_TestProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.Threads
{
    public class Author
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public Profile profile { get; set; }
        public bool is_lead { get; set; }
        public string role { get; set; }
        public bool is_followed { get; set; }
    }
}
