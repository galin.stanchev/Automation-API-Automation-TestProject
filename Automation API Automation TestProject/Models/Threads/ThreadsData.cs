﻿using Automation_API_Automation_TestProject.Models.CirclesGet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.Threads
{
    public class ThreadsData
    {
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string updated_ago { get; set; }
        public bool is_anonymous { get; set; }
        public bool is_liked { get; set; }
        public bool is_archived { get; set; }
        public bool is_followed { get; set; }
        public bool has_mentor_comments { get; set; }
        public Author author { get; set; }
        public Type type { get; set; }
        public Circle circle { get; set; }
        public List<Tag> tags { get; set; }
        public int comments_count { get; set; }
        public int likes_count { get; set; }
        public List<Attachment> attachments { get; set; }
    }
}
