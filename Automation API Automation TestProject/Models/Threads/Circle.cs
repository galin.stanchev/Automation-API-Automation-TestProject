﻿using Automation_API_Automation_TestProject.Models.CirclesGet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.Threads
{
    public class Circle
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<Tag> tags { get; set; }

    }
}
