﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.Threads
{
    public class Attachment
    {
        public string type { get; set; }
        public string link { get; set; }
    }
}
