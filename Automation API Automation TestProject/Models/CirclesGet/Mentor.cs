﻿using Automation_API_Automation_TestProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.CirclesGet
{
    public class Mentor
    {
        public int id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public bool is_completed { get; set; }
        public bool is_followed { get; set; }
        public string role { get; set; }
        public List<object> expertises { get; set; }
        public Profile profile { get; set; }
        public List<JoinedCommunity> joined_communities { get; set; }
        public List<object> owned_communities { get; set; }
        public LastCommunity last_community { get; set; }
        public List<MentorCircle> mentor_circles { get; set; }
        public List<JoinedMentorCircle> joined_mentor_circles { get; set; }
        public Counters counters { get; set; }
    }
}
