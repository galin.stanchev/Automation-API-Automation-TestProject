﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.CirclesGet
{
    public class Datum
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
        public int members_count { get; set; }
        public List<Thread> threads { get; set; }
        public List<Tag> tags { get; set; }
        public List<Mentor> mentors { get; set; }
    }
}
