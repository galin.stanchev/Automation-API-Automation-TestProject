﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.CirclesGet
{
    public class JoinedMentorCircle
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
