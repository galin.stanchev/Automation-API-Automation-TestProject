﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Models.CirclesGet
{
    public class Thread
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}
