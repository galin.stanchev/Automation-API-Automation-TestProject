﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Model
{
    public class Counters
    {
        public int followers { get; set; }
        public int followed { get; set; }
        public int circles { get; set; }
        public int archived { get; set; }
    }
}
