﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.Model
{
    public class Profile
    {
        public string profile_picture { get; set; }
        public string title { get; set; }
        public string department { get; set; }
        public string company { get; set; }
        public string location { get; set; }
        public string biography { get; set; }
        public string skills { get; set; }
        public string linkedin { get; set; }
        public string facebook { get; set; }
        public string instagram { get; set; }
    }
}
