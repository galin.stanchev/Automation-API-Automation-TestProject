﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_API_Automation_TestProject.EndPoints
{
    public static class EndpointsKeeper
    {       
        public static string postRegisterUrl = "http://bb-training.test.devsmm.com/api/register";
        public static string postLoginUrl = "http://bb-training.test.devsmm.com/api/auth/login";
        public static string getUsersMeUrl = "http://bb-training.test.devsmm.com/api/users/me";
        public static string postLogout = "http://bb-training.test.devsmm.com/api/auth/logout";
        public static string postSetUpProfile = "http://bb-training.test.devsmm.com/api/register/setup";
        public static string getCirclesGet = "http://bb-training.test.devsmm.com/api/circles";
        public static string postSetUpInterests = "http://bb-training.test.devsmm.com/api/register/interests";
        public static string getCorrectSavedInfoCheck = "http://bb-training.test.devsmm.com/api/circles?filter=joined";
        public static string putPasswordChange = "http://bb-training.test.devsmm.com/api/passwords/change";
        public static string getCirclesNotJoined = "http://bb-training.test.devsmm.com/api/circles?filter=not_joined";
        public static string postJoinSecondCircle = "http://bb-training.test.devsmm.com/api/followings/";     
        public static string getThreadType = "http://bb-training.test.devsmm.com/api/thread-types";
        
        public static string putTags = "http://bb-training.test.devsmm.com/api/threads/";
      
       

    }
}
